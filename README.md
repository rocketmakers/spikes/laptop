# Laptop

---

## Core


---

### brew
> The Missing Package Manager for macOS (or Linux)
#### install
```
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"
```
#### documentation
https://brew.sh/

---

### iTerm
> iTerm2 is a replacement for Terminal and the successor to iTerm
#### install
```
brew cask install iterm2
```
#### documentation
https://www.iterm2.com/

---

### zsh
#### install
```
brew install zsh
```
#### documentation
https://ohmyz.sh/
https://towardsdatascience.com/the-ultimate-guide-to-your-terminal-makeover-e11f9b87ac99
#### configuration
```
chsh -s /bin/zsh
sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
chmod 755 /usr/local/share/zsh
chmod 755 /usr/local/share/zsh/site-functions
```

---

### starship
> zsh shell prompt
#### install
```
brew install starship
```
#### documentation
https://starship.rs/
#### configuration
```
echo 'eval "$(starship init zsh)"' >> ~/.zshrc
```


---

## Communication


---

### Slack
#### install
https://slack.com/intl/en-gb/downloads/mac?geocode=en-gb


---

## Browser


---

### Chrome
#### install
https://www.google.co.uk/chrome

---

### Firefox
#### install
https://www.mozilla.org/en-GB/firefox/new/


---

## Development


---

### git
#### install
```
brew install git
```

---

### nodenv
> node environment manager
#### install
```
brew install nodenv
```
#### configuration
```
echo 'eval "$(nodenv init -)"' >> ~/.zshrc
```

---

### tfenv
> terraform environment manager
#### install
```
brew install tfenv
```

---

### azure-cli
> Azure Command Line Interface
#### install
```
brew install azure-cli
```
#### documentation
https://docs.microsoft.com/en-us/cli/azure/

---

### kubectl
> Kubernetes Command Line Interface
#### install
```
brew install kubectl
```

---

### kubernetes-helm
> The package manager for Kubernetes
#### install
```
brew install kubernetes-helm
```
#### documentation
https://helm.sh/

---

### helmenv
> helm environment manager
#### install
```
git clone https://github.com/yuya-takeyama/helmenv.git ~/.helmenv
echo 'export PATH="$HOME/.helmenv/bin:$PATH"' >> ~/.zshrc
```
#### documentation
https://github.com/yuya-takeyama/helmenv

---

### skaffold
> Skaffold handles the workflow for building, pushing and deploying your kubernetes application
#### install
```
brew install skaffold
```
#### documentation
https://skaffold.dev/

---

### minikube
> implements a local Kubernetes cluster
#### install
```
brew install minikube hyperkit
```
#### documentation
https://github.com/kubernetes/minikube

---

### sops
> An editor of encrypted files
#### install
```
brew install sops gnu-getopt
```
#### documentation
https://github.com/mozilla/sops

---

### vault
> CLI for vault
#### install
```
brew install vault
```
#### documentation
https://www.vaultproject.io/

---

### ruby
> Ruby
#### install
```
brew install rbenv ruby-build
```
#### configuration
```
echo 'eval "$(rbenv init -)"' >> ~/.zshrc
```

---

### docker
> Containers - https://www.docker.com/why-docker
#### install
https://docs.docker.com/docker-for-mac/install/

---

### postman
> Test HTTP and GraphQL endpoints
#### install
https://www.postman.com/downloads/

---

### VSCode
#### install
https://code.visualstudio.com/download
#### configuration
```
After install:
In Code Menu: View -> Command Palette -> Shell Command: install 'code' command in PATH
```

---

### Azure Data Studio
#### install
https://go.microsoft.com/fwlink/?linkid=2148710
#### documentation
https://docs.microsoft.com/en-us/sql/azure-data-studio/what-is-azure-data-studio?view=sql-server-ver15


## Brew All-In-One
```
brew install zsh starship   git nodenv tfenv azure-cli kubectl kubernetes-helm skaffold minikube hyperkit sops gnu-getopt vault rbenv ruby-build
```