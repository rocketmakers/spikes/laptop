import { IGroupedInstallations } from "./types";

export const installations: IGroupedInstallations[] = [
  {
    section: "Core",
    installers: [
      {
        kind: "cmd",
        name: "brew",
        documentationUrls: ["https://brew.sh/"],
        description: "The Missing Package Manager for macOS (or Linux)",
        commands: [
          `/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"`,
        ],
      },
      {
        kind: "cmd",
        name: "iTerm",
        documentationUrls: ["https://www.iterm2.com/"],
        description:
          "iTerm2 is a replacement for Terminal and the successor to iTerm",
        commands: [`brew cask install iterm2`],
      },
      {
        kind: "brew",
        name: "zsh",
        command: `zsh`,
        configurations: [
          `chsh -s /bin/zsh`,
          `sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"`,
          `chmod 755 /usr/local/share/zsh`,
          `chmod 755 /usr/local/share/zsh/site-functions`,
        ],
        documentationUrls: [
          "https://ohmyz.sh/",
          "https://towardsdatascience.com/the-ultimate-guide-to-your-terminal-makeover-e11f9b87ac99",
        ],
      },
      {
        kind: "brew",
        name: "starship",
        description: "zsh shell prompt",
        command: `starship`,
        configurations: [`echo 'eval "$(starship init zsh)"' >> ~/.zshrc`],
        documentationUrls: ["https://starship.rs/"],
      },
    ],
  },
  {
    section: "Communication",
    installers: [
      {
        kind: "url",
        name: "Slack",
        url: "https://slack.com/intl/en-gb/downloads/mac?geocode=en-gb",
      },
    ],
  },
  {
    section: "Browser",
    installers: [
      { kind: "url", name: "Chrome", url: "https://www.google.co.uk/chrome" },
      {
        kind: "url",
        name: "Firefox",
        url: "https://www.mozilla.org/en-GB/firefox/new/",
      },
    ],
  },
  {
    section: "Development",
    installers: [
      { kind: "brew", name: "git", command: "git" },
      {
        kind: "brew",
        name: "nodenv",
        command: "nodenv",
        description: "node environment manager",
        configurations: [`echo 'eval "$(nodenv init -)"' >> ~/.zshrc`],
      },
      {
        kind: "brew",
        name: "tfenv",
        command: "tfenv",
        description: "terraform environment manager",
      },
      {
        kind: "brew",
        name: "azure-cli",
        command: "azure-cli",
        description: "Azure Command Line Interface",
        documentationUrls: ["https://docs.microsoft.com/en-us/cli/azure/"],
      },
      {
        kind: "brew",
        name: "kubectl",
        command: "kubectl",
        description: "Kubernetes Command Line Interface",
      },
      {
        kind: "brew",
        name: "kubernetes-helm",
        command: "kubernetes-helm",
        description: "The package manager for Kubernetes",
        documentationUrls: ["https://helm.sh/"],
      },
      {
        kind: "cmd",
        name: "helmenv",
        commands: [
          "git clone https://github.com/yuya-takeyama/helmenv.git ~/.helmenv",
          `echo 'export PATH="$HOME/.helmenv/bin:$PATH"' >> ~/.zshrc`,
        ],
        description: "helm environment manager",
        documentationUrls: ["https://github.com/yuya-takeyama/helmenv"],
      },
      {
        kind: "brew",
        name: "skaffold",
        command: "skaffold",
        description:
          "Skaffold handles the workflow for building, pushing and deploying your kubernetes application",
        documentationUrls: ["https://skaffold.dev/"],
      },
      {
        kind: "brew",
        name: "minikube",
        command: "minikube hyperkit",
        description: "implements a local Kubernetes cluster",
        documentationUrls: ["https://github.com/kubernetes/minikube"],
      },
      {
        kind: "brew",
        name: "sops",
        command: "sops gnu-getopt",
        description: "An editor of encrypted files",
        documentationUrls: ["https://github.com/mozilla/sops"],
      },
      {
        kind: "brew",
        name: "vault",
        command: "vault",
        description: "CLI for vault",
        documentationUrls: ["https://www.vaultproject.io/"],
      },
      {
        kind: "brew",
        name: "ruby",
        command: "rbenv ruby-build",
        description: "Ruby",
        //documentationUrls: ["https://www.vaultproject.io/"],
        configurations: [`echo 'eval "$(rbenv init -)"' >> ~/.zshrc`],
      },
      {
        kind: "url",
        name: "docker",
        description: "Containers - https://www.docker.com/why-docker",
        url: "https://docs.docker.com/docker-for-mac/install/",
      },
      {
        kind: "url",
        name: "postman",
        description: "Test HTTP and GraphQL endpoints",
        url: "https://www.postman.com/downloads/",
      },
      {
        kind: "url",
        name: "VSCode",
        url: "https://code.visualstudio.com/download",
        configurations: [
          `After install:`,
          `In Code Menu: View -> Command Palette -> Shell Command: install 'code' command in PATH`,
        ],
      },
      {
        kind: "url",
        name: "Azure Data Studio",
        url: "https://go.microsoft.com/fwlink/?linkid=2148710",
        documentationUrls: [
          "https://docs.microsoft.com/en-us/sql/azure-data-studio/what-is-azure-data-studio?view=sql-server-ver15",
        ],
      },
    ],
  },
];
