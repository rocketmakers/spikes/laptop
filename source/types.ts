export interface IInstall {
  name: string;
  description?: string;
  documentationUrls?: string[];
  configurations?: string[];
}

export interface ICommandInstall extends IInstall {
  kind: "cmd";
  commands: string[];
}

export interface IBrewInstall extends IInstall {
  kind: "brew";
  command: string;
}

export interface IUrlInstall extends IInstall {
  kind: "url";
  url: string;
}

export type Installation = ICommandInstall | IBrewInstall | IUrlInstall;

export type IGroupedInstallations = {
  section: string;
  installers: Installation[];
};
