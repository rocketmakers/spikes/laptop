import { writeFileSync } from "fs";
import { installations } from "./installations";
import {
  IGroupedInstallations,
  IInstall,
  Installation,
  IBrewInstall,
} from "./types";

const tripleTick = "```";
function heading4(text: string) {
  return `#### ${text}`;
}
function heading3(text: string) {
  return `### ${text}`;
}
function heading2(text: string) {
  return `## ${text}`;
}
function heading1(text: string) {
  return `# ${text}`;
}
function quote(text: string) {
  return `> ${text}`;
}
function head(installer: IInstall) {
  return [
    "\n---\n",
    `${heading3(installer.name)}`,
    installer.description ? quote(installer.description) : "",
  ].filter((t) => !!t);
}
function documentation(installer: IInstall) {
  const documentationUrls = installer.documentationUrls || [];
  return [
    documentationUrls.length ? heading4("documentation") : "",
    ...documentationUrls,
  ].filter((t) => !!t);
}
function foot(installer: IInstall) {
  if (installer.configurations && installer.configurations.length) {
    return [
      ...documentation(installer),
      `${heading4("configuration")}`,
      `${tripleTick}`,
      ...installer.configurations,
      `${tripleTick}`,
    ];
  }
  return documentation(installer);
}
function documentInstaller(installer: Installation) {
  switch (installer.kind) {
    case "brew":
      return [
        ...head(installer),
        heading4("install"),
        tripleTick,
        `brew install ${installer.command}`,
        tripleTick,
        ...foot(installer),
      ];
    case "cmd":
      return [
        ...head(installer),
        heading4("install"),
        tripleTick,
        ...installer.commands,
        tripleTick,
        ...foot(installer),
      ];
    case "url":
      return [
        ...head(installer),
        heading4("install"),
        installer.url,
        ...foot(installer),
      ];
    default:
      throw new Error("Unknown installer kind: " + installer);
  }
}

function documentInstallers(installers: Installation[]) {
  return installers.map((i) => documentInstaller(i).join("\n")).join("\n");
}

function groupedInstaller(doc: IGroupedInstallations) {
  return `
---

${heading2(doc.section)}

${documentInstallers(doc.installers)}
`;
}

function groupedInstallers(doc: IGroupedInstallations[]) {
  return `${heading1("Laptop")}
${doc.map(groupedInstaller).join("\n")}

${heading2("Brew All-In-One")}
${tripleTick}
brew install ${doc
    .map((d) =>
      d.installers
        .filter((f) => f.kind === "brew")
        .map((d: IBrewInstall) => d.command).filter(i => !!i)
        .join(" ")
    )
    .join(" ")}
${tripleTick}`;
}

writeFileSync("./README.md", groupedInstallers(installations));
